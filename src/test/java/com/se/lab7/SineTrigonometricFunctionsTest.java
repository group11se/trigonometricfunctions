package com.se.lab7;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * This class tests SineTrigonometricFunctions.java class using JUnit
 * 
 * @author GroupSE11
 *
 */
public class SineTrigonometricFunctionsTest {

	double delta = 0.001;
	int degree1 = 30, degree2 = 45, degree3 = 60, degree4 = 90, degree5 = 120, degree6 = 135, degree7 = 150,
			degree8 = 180, degree9 = 270, degree10 = 360;

	double radian1 = Math.PI / 6, radian2 = Math.PI / 4, radian3 = Math.PI / 3, radian4 = Math.PI / 2,
			radian5 = (2 * Math.PI) / 3, radian6 = (3 * Math.PI) / 4, radian7 = (5 * Math.PI) / 6,
			radian8 = Math.sin(Math.PI), radian9 = (3 * Math.PI) / 2, radian10 = 2 * Math.PI;

	@Test
	public void sineTestAngle1() {
		assertEquals(Math.sin(radian1), SineTrigonometricFunctions.sine(degree1), delta);
	}

	@Test
	public void sineTestAngle2() {
		assertEquals(Math.sin(-radian1), SineTrigonometricFunctions.sine(-degree1), delta);
	}

	@Test
	public void sineTestAngle3() {
		assertEquals(Math.sin(radian2), SineTrigonometricFunctions.sine(degree2), delta);
	}

	@Test
	public void sineTestAngle4() {
		assertEquals(Math.sin(-radian2), SineTrigonometricFunctions.sine(-degree2), delta);
	}

	@Test
	public void sineTestAngle5() {
		assertEquals(Math.sin(radian3), SineTrigonometricFunctions.sine(degree3), delta);
	}

	@Test
	public void sineTestAngle6() {
		assertEquals(Math.sin(-radian3), SineTrigonometricFunctions.sine(-degree3), delta);
	}

	@Test
	public void sineTestAngle7() {
		assertEquals(Math.sin(radian4), SineTrigonometricFunctions.sine(degree4), delta);
	}

	@Test
	public void sineTestAngle8() {
		assertEquals(Math.sin(-radian4), SineTrigonometricFunctions.sine(-degree4), delta);
	}

	@Test
	public void sineTestAngle9() {
		assertEquals(Math.sin(radian5), SineTrigonometricFunctions.sine(degree5), delta);
	}

	@Test
	public void sineTestAngle10() {
		assertEquals(Math.sin(-radian5), SineTrigonometricFunctions.sine(-degree5), delta);
	}

	@Test
	public void sineTestAngle11() {
		assertEquals(Math.sin(radian6), SineTrigonometricFunctions.sine(degree6), delta);
	}

	@Test
	public void sineTestAngle12() {
		assertEquals(Math.sin(-radian6), SineTrigonometricFunctions.sine(-degree6), delta);
	}

	@Test
	public void sineTestAngle13() {
		assertEquals(Math.sin(radian7), SineTrigonometricFunctions.sine(degree7), delta);
	}

	@Test
	public void sineTestAngle14() {
		assertEquals(Math.sin(-radian7), SineTrigonometricFunctions.sine(-degree7), delta);
	}

	@Test
	public void sineTestAngle15() {
		assertEquals(Math.sin(radian8), SineTrigonometricFunctions.sine(degree8), delta);
	}

	@Test
	public void sineTestAngle16() {
		assertEquals(Math.sin(-radian8), SineTrigonometricFunctions.sine(-degree8), delta);
	}

	@Test
	public void sineTestAngle17() {
		assertEquals(Math.sin(radian9), SineTrigonometricFunctions.sine(degree9), delta);
	}

	@Test
	public void sineTestAngle18() {
		assertEquals(Math.sin(-radian9), SineTrigonometricFunctions.sine(-degree9), delta);
	}

	@Test
	public void sineTestAngle19() {
		assertEquals(Math.sin(radian10), SineTrigonometricFunctions.sine(degree10), delta);
	}

	@Test
	public void sineTestAngle20() {
		assertEquals(Math.sin(-radian10), SineTrigonometricFunctions.sine(-degree10), delta);
	}

}
