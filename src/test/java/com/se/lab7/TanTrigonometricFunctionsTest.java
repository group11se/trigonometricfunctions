package com.se.lab7;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * This class tests TanTrigonometricFunctions.java class using JUnit
 * 
 * @author GroupSE11
 *
 */
public class TanTrigonometricFunctionsTest {

	double delta = 0.001;
	int degree1 = 30, degree2 = 45, degree3 = 60, degree4 = 90, degree5 = 120, degree6 = 135, degree7 = 150,
			degree8 = 180, degree9 = 270, degree10 = 360;

	double radian1 = Math.PI / 6, radian2 = Math.PI / 4, radian3 = Math.PI / 3, radian4 = Math.PI / 2,
			radian5 = (2 * Math.PI) / 3, radian6 = (3 * Math.PI) / 4, radian7 = (5 * Math.PI) / 6,
			radian8 = Math.sin(Math.PI), radian9 = (3 * Math.PI) / 2, radian10 = 2 * Math.PI;

	@Test
	public void tanTestAngle1() {
		assertEquals(Math.tan(radian1), TanTrigonometricFunctions.tan(degree1), delta);
	}

	@Test
	public void tanTestAngle2() {
		assertEquals(Math.tan(-radian1), TanTrigonometricFunctions.tan(-degree1), delta);
	}

	@Test
	public void tanTestAngle3() {
		assertEquals(Math.tan(radian2), TanTrigonometricFunctions.tan(degree2), delta);
	}

	@Test
	public void tanTestAngle4() {
		assertEquals(Math.tan(-radian2), TanTrigonometricFunctions.tan(-degree2), delta);
	}

	@Test
	public void tanTestAngle5() {
		assertEquals(Math.tan(radian3), TanTrigonometricFunctions.tan(degree3), delta);
	}

	@Test
	public void tanTestAngle6() {
		assertEquals(Math.tan(-radian3), TanTrigonometricFunctions.tan(-degree3), delta);
	}

	@Test
	public void tanTestAngle7() {
		assertEquals(Math.tan(radian5), TanTrigonometricFunctions.tan(degree5), delta);
	}

	@Test
	public void tanTestAngle8() {
		assertEquals(Math.tan(-radian5), TanTrigonometricFunctions.tan(-degree5), delta);
	}

	@Test
	public void tanTestAngle9() {
		assertEquals(Math.tan(radian6), TanTrigonometricFunctions.tan(degree6), delta);
	}

	@Test
	public void tanTestAngle10() {
		assertEquals(Math.tan(-radian6), TanTrigonometricFunctions.tan(-degree6), delta);
	}

	@Test
	public void tanTestAngle11() {
		assertEquals(Math.tan(radian7), TanTrigonometricFunctions.tan(degree7), delta);
	}

	@Test
	public void tanTestAngle12() {
		assertEquals(Math.tan(-radian7), TanTrigonometricFunctions.tan(-degree7), delta);
	}

	@Test
	public void tanTestAngle13() {
		assertEquals(Math.tan(radian8), TanTrigonometricFunctions.tan(degree8), delta);
	}

	@Test
	public void tanTestAngle14() {
		assertEquals(Math.tan(-radian8), TanTrigonometricFunctions.tan(-degree8), delta);
	}

	@Test
	public void tanTestAngle15() {
		assertEquals(Math.tan(radian10), TanTrigonometricFunctions.tan(degree10), delta);
	}

	@Test
	public void tanTestAngle16() {
		assertEquals(Math.tan(-radian10), TanTrigonometricFunctions.tan(-degree10), delta);
	}

}
