package com.se.lab7;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * This class tests CosineTrigonometricFunctions.java class using JUnit
 * 
 * @author GroupSE11
 *
 */
public class CosineTrigonometricFunctionsTest {

	double delta = 0.001;
	int degree1 = 30, degree2 = 45, degree3 = 60, degree4 = 90, degree5 = 120, degree6 = 135, degree7 = 150,
			degree8 = 180, degree9 = 270, degree10 = 360;

	double radian1 = Math.PI / 6, radian2 = Math.PI / 4, radian3 = Math.PI / 3, radian4 = Math.PI / 2,
			radian5 = (2 * Math.PI) / 3, radian6 = (3 * Math.PI) / 4, radian7 = (5 * Math.PI) / 6,
			radian8 = Math.sin(Math.PI), radian9 = (3 * Math.PI) / 2, radian10 = 2 * Math.PI;

	@Test
	public void cosineTestAngle1() {
		assertEquals(Math.cos(radian1), CosineTrigonometricFunctions.cosine(degree1), delta);
	}

	@Test
	public void cosineTestAngle2() {
		assertEquals(Math.cos(-radian1), CosineTrigonometricFunctions.cosine(-degree1), delta);
	}

	@Test
	public void cosineTestAngle3() {
		assertEquals(Math.cos(radian2), CosineTrigonometricFunctions.cosine(degree2), delta);
	}

	@Test
	public void cosineTestAngle4() {
		assertEquals(Math.cos(-radian2), CosineTrigonometricFunctions.cosine(-degree2), delta);
	}

	@Test
	public void cosineTestAngle5() {
		assertEquals(Math.cos(radian3), CosineTrigonometricFunctions.cosine(degree3), delta);
	}

	@Test
	public void cosineTestAngle6() {
		assertEquals(Math.cos(-radian3), CosineTrigonometricFunctions.cosine(-degree3), delta);
	}

	@Test
	public void cosineTestAngle7() {
		assertEquals(Math.cos(radian4), CosineTrigonometricFunctions.cosine(degree4), delta);
	}

	@Test
	public void cosineTestAngle8() {
		assertEquals(Math.cos(-radian4), CosineTrigonometricFunctions.cosine(-degree4), delta);
	}

	@Test
	public void cosineTestAngle9() {
		assertEquals(Math.cos(radian5), CosineTrigonometricFunctions.cosine(degree5), delta);
	}

	@Test
	public void cosineTestAngle10() {
		assertEquals(Math.cos(-radian5), CosineTrigonometricFunctions.cosine(-degree5), delta);
	}

	@Test
	public void cosineTestAngle11() {
		assertEquals(Math.cos(radian6), CosineTrigonometricFunctions.cosine(degree6), delta);
	}

	@Test
	public void cosineTestAngle12() {
		assertEquals(Math.cos(-radian6), CosineTrigonometricFunctions.cosine(-degree6), delta);
	}

	@Test
	public void cosineTestAngle13() {
		assertEquals(Math.cos(radian7), CosineTrigonometricFunctions.cosine(degree7), delta);
	}

	@Test
	public void cosineTestAngle14() {
		assertEquals(Math.cos(-radian7), CosineTrigonometricFunctions.cosine(-degree7), delta);
	}

	@Test
	public void cosineTestAngle15() {
		assertEquals(-1, CosineTrigonometricFunctions.cosine(degree8), delta);
	}

	@Test
	public void cosineTestAngle16() {
		assertEquals(-1, CosineTrigonometricFunctions.cosine(-degree8), delta);
	}

	@Test
	public void cosineTestAngle17() {
		assertEquals(Math.cos(radian9), CosineTrigonometricFunctions.cosine(degree9), delta);
	}

	@Test
	public void cosineTestAngle18() {
		assertEquals(Math.cos(-radian9), CosineTrigonometricFunctions.cosine(-degree9), delta);
	}

	@Test
	public void cosineTestAngle19() {
		assertEquals(Math.cos(radian10), CosineTrigonometricFunctions.cosine(degree10), delta);
	}

	@Test
	public void cosineTestAngle20() {
		assertEquals(Math.cos(-radian10), CosineTrigonometricFunctions.cosine(-degree10), delta);
	}
}
