package com.se.lab7;

import java.text.DecimalFormat;

/**
 * TrigonometryFunctions uses the taylor series for implementing the
 * trigonometric sin, cosine and tan functions.
 * 
 * @Version: 3.0
 * @author GroupSE11
 *
 */
public class TanTrigonometricFunctions {

	static final double pi = 3.141592653589793;
	static double sum;
	static double factorial;
	static double power;;

	/**
	 * This class calculates value for Tan trignometric functions and value is
	 * compared with Math library functions.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		DecimalFormat dformat = new DecimalFormat();
		dformat.setMaximumFractionDigits(7);

		System.out.println("Tan(30) is " + dformat.format(Math.tan(Math.PI / 6)) + " " + dformat.format(tan(30)));
		System.out.println("Tan(45) is " + dformat.format(Math.tan(Math.PI / 4)) + " " + dformat.format(tan(45)));
		System.out.println("Tan(60) is " + dformat.format(Math.tan(Math.PI / 3)) + " " + dformat.format(tan(60)));
		System.out
				.println("Tan(120) is " + dformat.format(Math.tan((2 * Math.PI) / 3)) + " " + dformat.format(tan(120)));
		System.out
				.println("Tan(135) is " + dformat.format(Math.tan((3 * Math.PI) / 4)) + " " + dformat.format(tan(135)));
		System.out
				.println("Tan(150) is " + dformat.format(Math.tan((5 * Math.PI) / 6)) + " " + dformat.format(tan(150)));
		System.out.println("Tan(180) is " + dformat.format(Math.tan(Math.PI)) + " " + dformat.format(tan(180)));
		System.out.println("Tan(360) is " + dformat.format(Math.tan(2 * Math.PI)) + " " + dformat.format(tan(360)));

	}

	/**
	 * This method calculates degree for sin function and it is implemented without
	 * Math library provided by java Taylor series is considered for this
	 * implementation It accepts degree and converts it to radian and returns final
	 * value
	 * 
	 * @param degree
	 * @return
	 */
	static double sine(double degree) {
		double radian = degreeToRadian(degree);
		sum = 0.0;
		for (int i = 0; i <= 20; i++) {
			factorial = 1.0;
			power = 1.0;
			for (int j = 1; j <= 2 * i + 1; j++) {
				factorial *= j;
				power *= radian;
			}
			sum += ((i % 2 == 0 ? 1.0 : -1.0) / factorial) * power;
		}
		return sum;
	}

	/**
	 * This method calculates degree for cosine function and it is implemented
	 * without Math library provided by java Taylor series is considered for this
	 * implementation It accepts degree and converts it to radian and returns final
	 * value
	 * 
	 * @param degree
	 * @return
	 */
	static double cosine(double degree) {
		double radian = degreeToRadian(degree);
		sum = 0.0;
		for (int i = 0; i <= 20; i++) {
			factorial = 1.0;
			power = 1.0;
			for (int j = 1; j <= 2 * i; j++) {
				factorial *= j;
				power *= radian;
			}
			sum += (((i % 2 == 0) ? 1.0 : -1.0) / factorial) * power;
		}

		return sum;
	}

	/**
	 * Converts degree to radian
	 * 
	 * @param degree
	 * @return
	 */
	static double degreeToRadian(double degree) {
		return (degree * pi) / 180;
	}

	/**
	 * This method calculates degree for tan function and it is implemented
	 * without Math library provided by trignometric equations is considered for this
	 * implementation It accepts degree as value
	 * value
	 * @param x
	 * @return
	 */
	static double tan(double x) {
		return (sine(x) / cosine(x));
	}

}
