package com.se.lab7;

import java.text.DecimalFormat;

/**
 * TrigonometryFunctions uses the taylor series for implementing the
 * trigonometric sine functions.
 * 
 * @Version: 1.0
 * @author GroupSE11
 *
 */
public class SineTrigonometricFunctions {

	static final double pi = 3.141592653589793;
	static double sum;
	static double factorial;
	static double power;

	/**
	 * This class calculates value for sine trignometric functions and value is
	 * compared with Math library functions.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		DecimalFormat dformat = new DecimalFormat();
		dformat.setMaximumFractionDigits(7);

		System.out.println("Sine(30) is " + dformat.format(Math.sin(Math.PI / 6)) + " " + dformat.format(sine(30)));
		System.out.println("Sine(30) is " + dformat.format(Math.sin(Math.PI / 6)) + " " + dformat.format(sine(30)));
		System.out.println("Sine(45) is " + dformat.format(Math.sin(Math.PI / 4)) + " " + dformat.format(sine(45)));
		System.out.println("Sine(60) is " + dformat.format(Math.sin(Math.PI / 3)) + " " + dformat.format(sine(60)));
		System.out.println("Sine(90) is " + dformat.format(Math.sin(Math.PI / 2)) + " " + dformat.format(sine(90)));
		System.out.println(
				"Sine(120) is " + dformat.format(Math.sin((2 * Math.PI) / 3)) + " " + dformat.format(sine(120)));
		System.out.println(
				"Sine(135) is " + dformat.format(Math.sin((3 * Math.PI) / 4)) + " " + dformat.format(sine(135)));
		System.out.println(
				"Sine(150) is " + dformat.format(Math.sin((5 * Math.PI) / 6)) + " " + dformat.format(sine(150)));
		System.out.println("Sine(180) is " + dformat.format(Math.sin(Math.PI)) + " " + dformat.format(sine(180)));
		System.out.println(
				"Sine(270) is " + dformat.format(Math.sin((3 * Math.PI) / 2)) + " " + dformat.format(sine(270)));
		System.out.println("Sine(360) is " + dformat.format(Math.sin(2 * Math.PI)) + " " + dformat.format(sine(360)));
	}

	/**
	 * This method calculates degree for sin function and it is implemented without
	 * Math library provided by java Taylor series is considered for this
	 * implementation It accepts degree and converts it to radian and returns final
	 * value
	 * 
	 * @param degree
	 * @return
	 */
	static double sine(double degree) {
		double radian = degreeToRadian(degree);
		sum = 0.0;
		for (int i = 0; i <= 20; i++) {
			factorial = 1.0;
			power = 1.0;
			for (int j = 1; j <= 2 * i + 1; j++) {
				factorial *= j;
				power *= radian;
			}
			sum += ((i % 2 == 0 ? 1.0 : -1.0) / factorial) * power;
		}
		return sum;
	}

	/**
	 * Converts degree to radian
	 * 
	 * @param degree
	 * @return
	 */
	static double degreeToRadian(double degree) {
		return (degree * pi) / 180;
	}

}
