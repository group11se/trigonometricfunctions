package com.se.lab7;

import java.text.DecimalFormat;

/**
 * TrigonometryFunctions uses the taylor series for implementing the
 * trigonometric cosine functions.
 * 
 * @Version: 2.0
 * @author GroupSE11
 *
 */
public class CosineTrigonometricFunctions {

	static final double pi = 3.141592653589793;
	static double sum;
	static double factorial;
	static double power;

	/**
	 * This class calculates value for cosine trignometric functions and value is
	 * compared with Math library functions.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		DecimalFormat dformat = new DecimalFormat();
		dformat.setMaximumFractionDigits(7);

		System.out.println("Cosine(30) is " + dformat.format(Math.cos(Math.PI / 6)) + " " + dformat.format(cosine(30)));
		System.out.println("Cosine(45) is " + dformat.format(Math.cos(Math.PI / 4)) + " " + dformat.format(cosine(45)));
		System.out.println("Cosine(60) is " + dformat.format(Math.cos(Math.PI / 3)) + " " + dformat.format(cosine(60)));
		System.out.println("Cosine(90) is " + dformat.format(Math.cos(Math.PI / 2)) + " " + dformat.format(cosine(90)));
		System.out.println(
				"Cosine(120) is " + dformat.format(Math.cos((2 * Math.PI) / 3)) + " " + dformat.format(cosine(120)));
		System.out.println(
				"Cosine(135) is " + dformat.format(Math.cos((3 * Math.PI) / 4)) + " " + dformat.format(cosine(135)));
		System.out.println(
				"Cosine(150) is " + dformat.format(Math.cos((5 * Math.PI) / 6)) + " " + dformat.format(cosine(150)));
		System.out.println("Cosine(180) is " + dformat.format(Math.sin(Math.PI)) + " " + dformat.format(cosine(180)));
		System.out.println(
				"Cosine(270) is " + dformat.format(Math.cos((3 * Math.PI) / 2)) + " " + dformat.format(cosine(270)));
		System.out
				.println("Cosine(360) is " + dformat.format(Math.cos(2 * Math.PI)) + " " + dformat.format(cosine(360)));

	}

	/**
	 * This method calculates degree for cosine function and it is implemented
	 * without Math library provided by java Taylor series is considered for this
	 * implementation It accepts degree and converts it to radian and returns final
	 * value
	 * 
	 * @param degree
	 * @return
	 */
	static double cosine(double degree) {
		double radian = degreeToRadian(degree);
		sum = 0.0;
		for (int i = 0; i <= 20; i++) {
			factorial = 1.0;
			power = 1.0;
			for (int j = 1; j <= 2 * i; j++) {
				factorial *= j;
				power *= radian;
			}
			sum += (((i % 2 == 0) ? 1.0 : -1.0) / factorial) * power;
		}

		return sum;
	}

	/**
	 * Converts degree to radian
	 * 
	 * @param degree
	 * @return
	 */
	static double degreeToRadian(double degree) {
		return (degree * pi) / 180;
	}

}
